import * as vscode from "vscode";

export const getNonce = () => {
	let text = '';
	const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	for (let i = 0; i < 32; i++) {
		text += possible.charAt(Math.floor(Math.random() * possible.length));
	}
	return text;
};

export class Rez2DSpritesheetEditorProvider implements vscode.CustomTextEditorProvider {
    private static readonly viewType = 'rez2d.spritesheet';

    public static register(context: vscode.ExtensionContext): vscode.Disposable {
        const provider = new Rez2DSpritesheetEditorProvider(context);
        const providerRegistration = vscode.window.registerCustomEditorProvider(Rez2DSpritesheetEditorProvider.viewType, provider);
        return providerRegistration;
    }

    constructor(private readonly context: vscode.ExtensionContext) {}

    public async resolveCustomTextEditor(
        document: vscode.TextDocument,
        webviewPanel: vscode.WebviewPanel,
        _token: vscode.CancellationToken
    ): Promise<void> {
        webviewPanel.webview.options = {
            enableScripts: true,
        };
        webviewPanel.webview.html = this.getHTML(webviewPanel.webview);

        const updateView = () => {
            webviewPanel.webview.postMessage({
                type: "update",
                text: document.getText(),
            });
        }

        const sub = vscode.workspace.onDidChangeTextDocument((e) => {
            if (e.document.uri.toString() === document.uri.toString()) {
                updateView();
            }
        });

        webviewPanel.onDidDispose(() => {
            sub.dispose();
        });

        updateView();
    }

    private getHTML(webview: vscode.Webview): string {
        const scriptURI = webview.asWebviewUri(
            vscode.Uri.joinPath(
                this.context.extensionUri,
                "dist/editors/spritesheet.js",
            )
        );
        const styleURI = webview.asWebviewUri(
            vscode.Uri.joinPath(
                this.context.extensionUri,
                "dist/editors/spritesheet.css",
            )
        )

        const nonce = getNonce();

        return `
                <!DOCTYPE html>
                <html lang="en">
                <head>
                    <meta charset="UTF-8">
                    <meta http-equiv="Content-Security-Policy" content="default-src 'none'; img-src ${webview.cspSource}; style-src ${webview.cspSource}; script-src 'nonce-${nonce}';">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <link href="${styleURI}" rel="stylesheet" />
                </head>
                <body>
                    <div id="main" role="main"></div>
                    <script nonce="${nonce}" src="${scriptURI}"></script>
                </body>
                </html>
        `;
    }

    private updateJSON(document: vscode.TextDocument, json: string) {
        const edit = new vscode.WorkspaceEdit();
        edit.replace(document.uri, new vscode.Range(0, 0, document.lineCount, 0), json);
        return vscode.workspace.applyEdit(edit);
    }
}
