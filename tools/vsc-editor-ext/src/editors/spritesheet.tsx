import {
    render,
    h,
    FunctionComponent,
    Fragment,
} from "preact";
import { useState, useEffect } from "preact/hooks";
import type { Spritesheet } from "../types";
import "./spritesheet.css";

// @ts-expect-error
const vscode = acquireVsCodeApi();

// On initial load we receive the message with the file data before the initial
// preact render is complete, so we need to store it in vscode state to make it
// available to components.
window.addEventListener("message", (event) => {
    if (!vscode.getState() && event.data.type === "update") {
        vscode.setState(JSON.parse(event.data.text));
    }
});

const Editor: FunctionComponent = () => {
    const [sheet, setSheet] = useState<Spritesheet>({ source: "", sprites: [], animations: [] });

    const onMessage = (event: MessageEvent) => {
        switch (event.data.type) {
            case "update":
                const parsed = JSON.parse(event.data.text);
                setSheet(parsed);
                vscode.setState(parsed);
                break;
        }
    };

    useEffect(() => {
        const initialState = vscode.getState();
        if (initialState) {
            setSheet(initialState);
        }

        window.addEventListener("message", onMessage);

        return () => {
            window.removeEventListener("message", onMessage);
        };
    }, []);

    return (
        <>
            <h1 className="text-3xl font-bold">Source: {sheet.source}</h1>
        </>
    );
};

render(<Editor />, document.getElementById("main") as HTMLElement);
