const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const baseConfig = {
    target: "node",
    mode: "none",
    devtool: "nosources-source-map",
    externals: {
        vscode: "commonjs vscode",
    },
    resolve: {
        extensions: [".ts", ".js"],
    },
    module: {
        rules: [
            {
                test: /\.ts[x]?$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: "ts-loader",
                    },
                ],
            },
        ],
    },
    infrastructureLogging: {
        level: "log",
    },
};

module.exports = [
    {
        ...baseConfig,
        entry: "./src/extension.ts",
        output: {
            path: path.resolve(__dirname, "dist"),
            filename: "extension.js",
            libraryTarget: "commonjs2",
        },
    },
    {
        ...baseConfig,
        entry: "./src/editors/spritesheet.tsx",
        output: {
            path: path.resolve(__dirname, "dist", "editors"),
            filename: "spritesheet.js",
        },
        module: {
            rules: [
                ...baseConfig.module.rules,
                {
                    test: /\.css$/,
                    use: [MiniCssExtractPlugin.loader, "css-loader", "postcss-loader"],
                },
            ]
        },
        plugins: [
            new MiniCssExtractPlugin({
                filename: "spritesheet.css",
            }),
        ],
    },
];
