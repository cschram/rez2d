import { Vec } from "ella-math";

export enum Key {
    BACKSPACE = "Backspace",
    TAB = "Tab",
    ENTER = "Enter",
    SHIFT_LEFT = "ShiftLeft",
    SHIFT_RIGHT = "ShiftRight",
    CTRL_LEFT = "ControlLeft",
    CTRL_RIGHT = "ControlRight",
    ALT_LEFT = "AltLeft",
    ALT_RIGHT = "AltRight",
    CAPSLOCK = "CapsLock",
    ESCAPE = "Escape",
    SPACEBAR = "Spacebar",
    LEFT = "ArrowLeft",
    UP = "ArrowUp",
    RIGHT = "ArrowRight",
    DOWN = "ArrowDown",
    ZERO = "Digit0",
    ONE = "Digit1",
    TWO = "Digit2",
    THREE = "Digit3",
    FOUR = "Digit4",
    FIVE = "Digit5",
    SIX = "Digit6",
    SEVEN = "Digit7",
    EIGHT = "Digit8",
    NINE = "Digit9",
    A = "KeyA",
    B = "KeyB",
    C = "KeyC",
    D = "KeyD",
    E = "KeyE",
    F = "KeyF",
    G = "KeyG",
    H = "KeyH",
    I = "KeyI",
    J = "KeyJ",
    K = "KeyK",
    L = "KeyL",
    M = "KeyM",
    N = "KeyN",
    O = "KeyO",
    P = "KeyP",
    Q = "KeyQ",
    R = "KeyR",
    S = "KeyS",
    T = "KeyT",
    U = "KeyU",
    V = "KeyV",
    W = "KeyW",
    X = "KeyX",
    Y = "KeyY",
    Z = "KeyZ",
    NUM0 = "Numpad0",
    NUM1 = "Numpad1",
    NUM2 = "Numpad2",
    NUM3 = "Numpad3",
    NUM4 = "Numpad4",
    NUM5 = "Numpad5",
    NUM6 = "Numpad6",
    NUM7 = "Numpad7",
    NUM8 = "Numpad8",
    NUM9 = "Numpad9",
    MULTIPlY = "NumpadMultiply",
    ADD = "NumpadAdd",
    SUBTRACT = "NumpadSubtract",
    DECIMALPOINT = "NumpadDecimal",
    DIVIDE = "NumpadDivide",
    SEMICOLON = "Semicolon",
    EQUAL = "Equal",
    COMMA = "Comma",
    DASH = "Minus",
    PERIOD = "Period",
    FORWARDSLASH = "Slash",
    TACK = "Backquote",
    OPENBRACKET = "BracketLeft",
    BACKSLASH = "Backslash",
    CLOSEBRACKET = "BracketRight",
    SINGLEQUOTE = "Quote"
}

export enum MouseButton {
    LEFT,
    MIDDLE,
    RIGHT
}

export class Input {
    private container: HTMLCanvasElement;
    private keyMap: Record<string, boolean>;
    private mouseButtonMap: Record<number, boolean>;
    private mousePosition: Vec;

    constructor(container: HTMLCanvasElement) {
        this.container = container;
        this.keyMap = {};
        this.mouseButtonMap = {};
        this.mousePosition = new Vec(0, 0);


        document.addEventListener("keydown", (e) => {
            this.keyMap[e.code] = true;
        });
        document.addEventListener("keyup", (e) => {
            this.keyMap[e.code] = false;
        });
        document.addEventListener("mousedown", (e) => {
            this.mouseButtonMap[e.button] = true;
        });
        document.addEventListener("mouseup", (e) => {
            this.mouseButtonMap[e.button] = false;
        });
        document.addEventListener("mousemove", (e) => {
            const canvasRect = this.container.getBoundingClientRect();
            const scaleX = this.container.width / canvasRect.width;
            const scaleY = this.container.height / canvasRect.height;
            this.mousePosition = new Vec(
                (e.pageX - canvasRect.left) * scaleX,
                (e.pageY - canvasRect.top) * scaleY
            );
        });
    }

    isKeyDown(key: Key): boolean {
        return this.keyMap[key];
    }

    isMouseDown(button: MouseButton): boolean {
        return this.mouseButtonMap[button];
    }

    getMousePosition(): Vec {
        return this.mousePosition;
    }
}
