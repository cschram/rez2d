import { ECS, Module } from "@rez2d/ecs";
import { AudioModule } from "@rez2d/audio";
import { CollisionModule } from "@rez2d/collision";
import { InputModule } from "@rez2d/input";
import { RenderModule } from "@rez2d/render";
import { RezConfig, RezResources } from "./types";
import { AssetsModule } from "@rez2d/assets";

export class Rez<TResources = {}> {
    private _ecs: ECS<RezResources<TResources>>;

    constructor(config: RezConfig) {
        this._ecs = new ECS();
        this._ecs
            .addModule(AssetsModule({
                baseUrl: config.assetBaseUrl,
            }))
            .addModule(AudioModule())
            .addModule(CollisionModule(config))
            .addModule(InputModule(config))
            .addModule(RenderModule(config));
    }

    start(game: Module<RezResources<TResources>>) {
        this._ecs.addModule(game).start();
    }
}
