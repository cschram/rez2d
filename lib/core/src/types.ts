import { Module } from "@rez2d/ecs";
import { AssetsResources } from "@rez2d/assets";
import { AudioResources } from "@rez2d/audio";
import { CollisionResources } from "@rez2d/collision";
import { InputResources } from "@rez2d/input";
import { RenderResources } from "@rez2d/render";

export interface RezConfig {
    container: HTMLCanvasElement;
    width: number;
    height: number;
    worldSize: {
        width: number;
        height: number;
    };
    assetBaseUrl: string;
}

export type RezResources<TResources = {}> =
    & AssetsResources
    & AudioResources
    & CollisionResources
    & InputResources
    & RenderResources
    & TResources;

export type RezModule<TResources = {}> = Module<RezResources<TResources>>;
