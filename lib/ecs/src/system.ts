import { Resources } from "./resources";
import { EntityStore } from "./store";

export type System<TResources> = (
    store: EntityStore<TResources>,
    resources: Resources<TResources>,
) => void;

export enum SystemPriority {
    HIGH,
    MEDIUM,
    LOW,
}

type SystemEntry<TResources> = [System<TResources>, SystemPriority];

export class SystemSchedule<TResources> {
    protected entries: SystemEntry<TResources>[];
    protected systems: System<TResources>[];

    constructor() {
        this.entries = [];
        this.systems = [];
    }

    add(system: System<TResources>, priority = SystemPriority.MEDIUM): SystemSchedule<TResources> {
        this.entries.push([system, priority]);
        this.sort();
        return this;
    }

    remove(system: System<TResources>): SystemSchedule<TResources> {
        this.entries = this.entries.filter(([sys, _]) => sys !== system);
        this.sort();
        return this;
    }

    run(store: EntityStore<TResources>, resources: Resources<TResources>) {
        this.systems.forEach((system) => {
            system(store, resources);
        });
    }

    private sort() {
        this.entries = this.entries.sort((a, b) => {
            if (a[1] > b[1]) {
                return 1;
            } else if (a[1] < b[1]) {
                return -1;
            } else {
                return 0;
            }
        });
        this.systems = this.entries.map(([system, _]) => system);
    }
}
