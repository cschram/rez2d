import { Option, Some, None } from "oxide.ts";

export class Resources<T = {}> {
    constructor(
        private res: Partial<T> = {}
    ) {}

    has<K extends keyof T>(key: K): boolean {
        return this.res[key] != undefined;
    }

    get<K extends keyof T>(key: K): Option<T[K]> {
        if (this.has(key)) {
            return Some(this.res[key] as T[K]);
        } else {
            return None;
        }
    }

    set<K extends keyof T>(key: K, value: T[K]) {
        this.res[key] = value;
    }

    remove<K extends keyof T>(key: K) {
        this.res[key] = undefined;
    }

    clear() {
        this.res = {};
    }
}
