import { createComponent, createTag, IDGenerator } from "./component";
import { EventQueue } from "@rez2d/events";
import { Resources } from "./resources";

describe("createComponent", () => {
    let resources: Resources<{}>;

    afterEach(() => {
        IDGenerator.reset();
        resources = new Resources();
    });

    it("should create components with the correct mask", () => {
        const ComponentOne = createComponent(() => ({ one: true }));
        const ComponentTwo = createComponent(() => ({ two: true }));
        expect(ComponentOne.id).toBe(0);
        expect(ComponentTwo.id).toBe(1);
    });

    it("should be able to instantiate a component", () => {
        const component = createComponent(() => ({ test: true }));
        const instance = component.create()(resources);
        expect(instance.__id).toBe(component.id);
        expect(instance.__args).toBeUndefined();
        expect(instance.test).toBe(true);
    });

    it("should be able to instantiate a component with arguments", () => {
        const component = createComponent((_resources, args: { flag: boolean }) => ({
            test: args.flag,
        }));
        const instanceOne = component.create({ flag: false })(resources);
        expect(instanceOne.__args).toStrictEqual({ flag: false });
        const instanceTwo = component.create({ flag: true })(resources);
        expect(instanceTwo.__args).toStrictEqual({ flag: true });
    });
});

describe("createTag", () => {
    let resources: Resources<{}>;

    afterEach(() => {
        IDGenerator.reset();
        resources = new Resources();
    });

    it("should create a tag component", () => {
        const Tag = createTag("test");
        const instance = Tag.create()(resources);
        expect(instance.__id).toBe(0);
        expect(instance.tag_test).toBe(true);
    });
});
