import { Option, match, Some, None } from "oxide.ts";
import { SlotMap, SecondaryMap, SlotKey } from "@rez2d/slotmap";
import { BitSet } from "@rez2d/bitset";
import { Commands, CommandType } from "./commands";
import {
    Component,
    ComponentFactory,
    ComponentId,
} from "./component";
import { EntityEntry } from "./entry";
import {
    Query,
    QueryResult,
    QueryResultIterator,
    InferComponentTuple,
    QUERY_MASK_SIZE,
} from "./query";
import { Resources } from "./resources";

export type EntityId = SlotKey;

export type EntityStoreExecFn<TResources = {}> =
    (commands: Commands<TResources>) => void;

/**
 * Storage container for entities and their associated components.
 */
export class EntityStore<TResources = {}> {
    private entities: SlotMap<BitSet>;
    private componentMap: Map<
        ComponentId,
        SecondaryMap<Component<unknown, unknown, TResources>>
    >;

    constructor() {
        this.entities = new SlotMap();
        this.componentMap = new Map();
    }

    /**
     * Get and entity entry by ID.
     * @param entityId ID of the entity you're retrieving a bitmask for.
     * @returns The entity entry or None if it does not exist.
     */
    get(entityId: EntityId): Option<EntityEntry> {
        return this.entities.get(entityId)
            .map((mask) => {
                const components = new Map();
                for (let i = 0; i < QUERY_MASK_SIZE; i++) {
                    if (mask.get(i)) {
                        // NOTE: In theory if an entity exists with a component bit set,
                        // the component should always exist here. But if for some reason it
                        // doesn't this is going to break stuff.
                        components.set(i, this.componentMap.get(i)?.get(entityId).unwrap());
                    }
                }
                return new EntityEntry(entityId, mask, components);
            });
    }

    has(entityId: EntityId, component: ComponentFactory<unknown, unknown, TResources>): boolean {
        return match(this.entities.get(entityId), {
            Some: (mask) => {
                return mask.get(component.id).unwrap();
            },
            None: () => false,
        });
    }

    exec(
        resources: Resources<TResources>,
        fn: EntityStoreExecFn<TResources>,
    ) {
        const commands = new Commands<TResources>();
        fn(commands);
        this.commit(resources, commands);
    }

    /**
     * Perform a query for entities and their components.
     * @param query Query object to operate on.
     * @returns Results of the search.
     */
    *query<TComponents extends [...ComponentFactory<unknown, unknown, TResources>[]]>(
        query: Query<TResources, TComponents>
    ): QueryResultIterator<TResources, TComponents> {
        for (const [entity, entityMask] of this.entities) {
            if (query.matches(entityMask)) {
                const entityComponents = query.components.map((component) => {
                    const map = this.componentMap
                        .get(component.id) as SecondaryMap<Component<unknown, unknown, TResources>>;
                    return map.get(entity).unwrap();
                });
                yield [
                    entity,
                    ...entityComponents
                ] as QueryResult<TResources, InferComponentTuple<TComponents>>;
            }
        }
    }

    queryFirst<TComponents extends [...ComponentFactory<unknown, unknown, TResources>[]]>(
        query: Query<TResources, TComponents>,
    ): Option<QueryResult<TResources, InferComponentTuple<TComponents>>> {
        for (const [entity, entityMask] of this.entities) {
            if (query.matches(entityMask)) {
                const entityComponents = query.components.map((component) => {
                    const map = this.componentMap
                        .get(component.id) as SecondaryMap<Component<unknown, unknown, TResources>>;
                    return map.get(entity).unwrap();
                });
                return Some(
                    [
                        entity,
                        ...entityComponents
                    ] as QueryResult<TResources, InferComponentTuple<TComponents>>
                );
            }
        }
        return None;
    }

    private commit(
        resources: Resources<TResources>,
        commands: Commands<TResources>
    ) {
        for (const command of commands) {
            switch (command.type) {
                case CommandType.CREATE_ENTITY:
                    const mask = new BitSet(QUERY_MASK_SIZE);
                    const id = this.entities.insert(mask);
                    command.components.forEach((fn) => {
                        // Instantiate component from intermediate
                        const component = fn(resources);
                        // Enable component in entity mask
                        mask.set(component.__id);
                        // Insert component into map
                        const components = this.componentMap.get(component.__id) || new SecondaryMap();
                        components.insert(id, component);
                        this.componentMap.set(component.__id, components);
                    });
                    break;
                case CommandType.DELETE_ENTITY:
                    match(this.entities.get(command.entityId), {
                        Some: (mask) => {
                            // Iterate over all the entities components and call their
                            // deconstructor function.
                            for (const cid of mask) {
                                const components = this.componentMap.get(cid);
                                if (components) {
                                    // Call components deconstructor function.
                                    match(components.get(command.entityId), {
                                        Some: (component) => {
                                            component.destroy(resources);
                                        },
                                        None: () => {},
                                    });
                                    // Remove the component
                                    components.remove(command.entityId);
                                }
                            }
                            this.entities.remove(command.entityId);
                        },
                        None: () => {},
                    });
                    break;
                case CommandType.ADD_COMPONENTS:
                    match(this.entities.get(command.entityId), {
                        Some: (mask) => {
                            command.components.forEach((fn) => {
                                // Instantiate component from intermediate
                                const component = fn(resources);
                                // Enable component in entity mask
                                mask.set(component.__id);
                                // Insert component into map
                                const components = this.componentMap.get(component.__id) || new SecondaryMap();
                                components.insert(command.entityId, component);
                                this.componentMap.set(component.__id, components);
                            });
                        },
                        None: () => {},
                    });
                    break;
                case CommandType.REMOVE_COMPONENTS:
                    match(this.entities.get(command.entityId), {
                        Some: (mask) => {
                            command.components.forEach((factory) => {
                                mask.unset(factory.id);
                                const components = this.componentMap.get(factory.id);
                                if (components) {
                                    // Call components deconstructor function.
                                    match(components.get(command.entityId), {
                                        Some: (component) => {
                                            component.destroy(resources);
                                        },
                                        None: () => {},
                                    });
                                    // Remove the component
                                    components.remove(command.entityId);
                                }
                            });
                        },
                        None: () => {},
                    });
                    break;
            }
        }
    }
}
