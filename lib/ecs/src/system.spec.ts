import { createComponent, IDGenerator } from "./component";
import { Resources } from "./resources";
import { EntityStore } from "./store";
import { SystemPriority, SystemSchedule } from "./system";

describe("SystemSchedule", () => {
    let store: EntityStore;
    let resources: Resources;
    let schedule: SystemSchedule<{}>;
    const TestComponentOne = createComponent(() => ({ one: true }));
    const TestSystemOne = jest.fn();
    const TestComponentTwo = createComponent(() => ({ two: true }));
    const TestSystemTwo = jest.fn();

    beforeEach(() => {
        IDGenerator.reset();
        store = new EntityStore();
        resources = new Resources();
        schedule = new SystemSchedule();
        TestSystemOne.mockClear();
        TestSystemTwo.mockClear();
        store.exec(resources, (commands) => {
            commands
                .create(TestComponentOne.create())
                .create(TestComponentTwo.create());
        });
    });

    it("should add and run systems", () => {
        schedule.add(TestSystemOne);
        schedule.add(TestSystemTwo);
        schedule.run(store, resources);
        expect(TestSystemOne).toHaveBeenCalledTimes(1);
        expect(TestSystemOne).toHaveBeenCalledWith(
            store,
            resources,
        );
        expect(TestSystemTwo).toHaveBeenCalledTimes(1);
        expect(TestSystemTwo).toHaveBeenCalledWith(
            store,
            resources,
        );
    });

    it("should remove systems", () => {
        schedule.add(TestSystemOne);
        schedule.add(TestSystemTwo);
        schedule.remove(TestSystemTwo);
        schedule.run(store, resources);
        expect(TestSystemOne).toHaveBeenCalledTimes(1);
        expect(TestSystemOne).toHaveBeenCalledWith(
            store,
            resources,
        );
        expect(TestSystemTwo).toHaveBeenCalledTimes(0);
    });

    it("should run systems in priority order", () => {
        const cb = jest.fn();
        schedule.add(() => cb(1));
        schedule.add(() => cb(2), SystemPriority.HIGH);
        schedule.run(store, resources);
        expect(cb).toHaveBeenCalledTimes(2);
        expect(cb.mock.calls[0][0]).toBe(2);
        expect(cb.mock.calls[1][0]).toBe(1);
    });
});
