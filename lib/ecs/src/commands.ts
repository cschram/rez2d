import { ComponentIntermediate, ComponentFactory } from "./component";
import { EntityId } from "./store";

export enum CommandType {
    CREATE_ENTITY,
    DELETE_ENTITY,
    ADD_COMPONENTS,
    REMOVE_COMPONENTS,
}

export interface CreateEntityCommand<TResources = {}> {
    type: CommandType.CREATE_ENTITY;
    components: ComponentIntermediate<unknown, unknown, TResources>[];
}

export interface DeleteEntityCommand {
    type: CommandType.DELETE_ENTITY;
    entityId: EntityId;
}

export interface AddComponentsCommand<TResources = {}> {
    type: CommandType.ADD_COMPONENTS;
    entityId: EntityId;
    components: ComponentIntermediate<unknown, unknown, TResources>[];
}

export interface RemoveComponentsCommand<TResources = {}> {
    type: CommandType.REMOVE_COMPONENTS;
    entityId: EntityId;
    components: ComponentFactory<unknown, unknown, TResources>[];
}

export type Command<TResources = {}> =
    | CreateEntityCommand<TResources>
    | DeleteEntityCommand
    | AddComponentsCommand<TResources>
    | RemoveComponentsCommand<TResources>;

/**
 * Commands class used to queue up entity and component mutations during system execution.
 * Mutating entity store state directly during system execution can lead to unexpected and
 * hard to debug behavior as well as exceptions. To avoid this mutations need to be queued up to
 * be committed after the system has run.
 */
export class Commands<TResources = {}> {
    private queue: Command<TResources>[];

    constructor() {
        this.queue = [];
    }

    /**
     * Create a new entity.
     * @param components Components to add to the newly created entity.
     * @returns Commands object.
     */
    create(
        ...components: ComponentIntermediate<unknown, unknown, TResources>[]
    ): Commands<TResources> {
        this.queue.push({
            type: CommandType.CREATE_ENTITY,
            components,
        });
        return this;
    }

    /**
     * Delete an entity.
     * @param entityId ID of the entity to delete.
     * @returns Commands object.
     */
    delete(entityId: EntityId): Commands<TResources> {
        this.queue.push({
            type: CommandType.DELETE_ENTITY,
            entityId,
        });
        return this;
    }

    /**
     * Add components to an entity.
     * @param entityId ID of the entity to add components to.
     * @param components Components to add to the entity.
     * @returns Commands object.
     */
    addComponents(
        entityId: EntityId,
        ...components: ComponentIntermediate<unknown, unknown, TResources>[]
    ): Commands<TResources> {
        this.queue.push({
            type: CommandType.ADD_COMPONENTS,
            entityId,
            components,
        });
        return this;
    }

    /**
     * Remove components from an entity.
     * @param entityId ID of the entity to remove components from.
     * @param components Components to remove from the entity.
     * @returns Commands object.
     */
    removeComponents(
        entityId: EntityId,
        ...components: ComponentFactory<unknown, unknown, TResources>[]
    ): Commands<TResources> {
        this.queue.push({
            type: CommandType.REMOVE_COMPONENTS,
            entityId,
            components,
        });
        return this;
    }

    *[Symbol.iterator](): IterableIterator<Command<TResources>> {
        for (const command of this.queue) {
            yield command;
        }
    }
}
