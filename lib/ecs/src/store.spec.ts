import { EntityStore } from "./store";
import { ComponentFactory, createComponent, IDGenerator } from "./component";
import { Commands } from "./commands";
import { Resources } from "./resources";
import { Query } from "./query";

describe("EntityStore", () => {
    let store: EntityStore;
    let resources: Resources;
    let ComponentOne: ComponentFactory<unknown>;
    let ComponentTwo: ComponentFactory<unknown>;
    let ComponentThree: ComponentFactory<unknown>;

    beforeEach(() => {
        IDGenerator.reset();
        store = new EntityStore();
        resources = new Resources();
        ComponentOne = createComponent(() => ({ one: true }));
        ComponentTwo = createComponent(() => ({ two: true }));
        ComponentThree = createComponent(() => ({ three: true }));
    });

    it("should execute a callback with commands", () => {
        const fn = jest.fn();
        store.exec(resources, fn);
        expect(fn).toBeCalledTimes(1);
        expect(fn).toBeCalledWith(new Commands());
    });

    it("should query entities", () => {
        store.exec(resources, (commands) => {
            commands
                .create(ComponentOne.create(), ComponentTwo.create(), ComponentThree.create())
                .create(ComponentOne.create(), ComponentThree.create())
                .create(ComponentTwo.create(), ComponentThree.create());
        });
        const results = store.query(new Query(ComponentOne, ComponentThree));
        expect(results.next().value?.length).toBe(3);
        expect(results.next().value?.length).toBe(3);
    });
});
