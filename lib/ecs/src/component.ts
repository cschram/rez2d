import { Resources } from "./resources";

export type ComponentId = number;

export const IDGenerator = (() => {
    let id = 0;
    const gen = (function*() {
        while (true) {
            yield id++;
        }
    })();
    return {
        get(): ComponentId {
            return gen.next().value;
        },
        reset() {
            id = 0;
        },
    };
})();

/**
 * Constructor function for a component. Called on instantiation to provide
 * component data. Any args given to ComponentFactory.create will be passed here.
 */
export type ComponentConstructor<TData, TArgs = void, TResources = {}> = (
    resources: Resources<TResources>,
    args: TArgs,
) => TData;

export type ComponentDeconstructor<TData, TArgs = void, TResources = {}> = (
    resources: Resources<TResources>,
    component: Component<TData, TArgs, TResources>,
) => void;

export type ComponentIntermediate<TData, TArgs = void, TResources = {}> = (
    resources: Resources<TResources>,
) => Component<TData, TArgs, TResources>;

/**
 * Instance of a component.
 */
export type Component<TData, TArgs = void, TResources = {}> = TData & {
    readonly __id: ComponentId;
    readonly __args: TArgs;
    destroy(
        resources: Resources<TResources>,
    ): void;
};

/**
 * Component factory, used to identify and instantiate components.
 */
export interface ComponentFactory<TData, TArgs = void, TResources = {}> {
    readonly id: ComponentId;
    create(args: TArgs): ComponentIntermediate<TData, TArgs, TResources>;
};

/**
 * Infer the component instance type from a component factory.
 */
export type InferComponent<TFactoryOrIntermediate> =
    TFactoryOrIntermediate extends ComponentFactory<infer TData, infer TArgs, infer TResources> ?
        Component<TData, TArgs, TResources> :
        TFactoryOrIntermediate extends ComponentIntermediate<infer TData, infer TArgs, infer TResources> ?
            Component<TData, TArgs, TResources> :
            never;

/**
 * Create a component factory.
 * @param ctr Constructor function called on instantiation to provide component data.
 * @param dtr Deconstructor function called when an instance is destroyed.
 * @returns A component factory.
 */
export function createComponent<TData, TArgs = void, TResources = {}>(
    ctr: ComponentConstructor<TData, TArgs, TResources>,
    dtr?: ComponentDeconstructor<TData, TArgs, TResources>,
): ComponentFactory<TData, TArgs, TResources> {
    const id = IDGenerator.get();
    return {
        id,
        create(args: TArgs) {
            return (resources: Resources<TResources>) => {
                return {
                    ...ctr(resources, args),
                    __id: id,
                    __args: args,
                    destroy(
                        resources: Resources<TResources>,
                    ) {
                        if (dtr) {
                            // @ts-expect-error
                            // TypeScript seems unable to infer at this point that this object is
                            // in fact a Component.
                            dtr(resources, this);
                        }
                    },
                };
            };
        },
    };
};

export function createTag(name: string) {
    return createComponent(() => ({ [`tag_${name}`]: true }));
}
