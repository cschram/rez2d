import { Option, None } from "oxide.ts";
import { ComponentFactory, createComponent, IDGenerator } from "./component";
import { ECS } from "./ecs";

describe("App", () => {
    let ecs: ECS<{ foo: string }>;
    let TestComponent: ComponentFactory<{ test: boolean }>;
    const TestSystem = jest.fn();

    beforeEach(() => {
        IDGenerator.reset();
        ecs = new ECS();
        TestComponent = createComponent(() => ({ test: true }));
    });

    it("should run systems", () => {
        ecs
            .addModule((store, _, resources) => {
                store.exec(resources, (commands) => {
                    commands.create(TestComponent.create());
                });
            })
            .addSystem(TestSystem)
            .start();
        ecs.runSchedule();
        expect(TestSystem).toBeCalledTimes(1);
        expect(TestSystem).toBeCalledWith(
            // @ts-expect-error
            ecs.store,
            // @ts-expect-error
            ecs.resources,
        );
    });

    it("should add resources", () => {
        let foo: Option<string> = None;
        ecs
            .addResource("foo", "bar")
            .addModule((_store, _schedule, resources) => {
                foo = resources.get("foo");
            })
            .start();
        expect(foo.unwrap()).toBe("bar");
    });

    it("should add modules", () => {
        const plugin = jest.fn();
        ecs.addModule(plugin).start();
        expect(plugin).toBeCalledTimes(1);
        expect(plugin).toBeCalledWith(
            // @ts-expect-error
            ecs.store,
            // @ts-expect-error
            ecs.schedule,
            // @ts-expect-error
            ecs.resources,
        );
    });
});
