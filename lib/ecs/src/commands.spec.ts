import { Commands, CommandType } from "./commands";
import { ComponentFactory, createComponent, IDGenerator } from "./component";
import { Resources } from "./resources";
import { SlotKey } from "@rez2d/slotmap";

describe("Commands", () => {
    let commands: Commands;
    let resources: Resources;
    let ComponentOne: ComponentFactory<any>;
    let ComponentTwo: ComponentFactory<any>;
    const fakeid: SlotKey = { index: 0, version: 1 };

    beforeEach(() => {
        IDGenerator.reset();
        commands = new Commands();
        resources = new Resources();
        ComponentOne = createComponent(() => ({ one: true }));
        ComponentTwo = createComponent(() => ({ two: true }));
    });

    it("should store and iterate over commands", () => {
        commands
            .create(ComponentOne.create(), ComponentTwo.create())
            .addComponents(fakeid, ComponentTwo.create())
            .removeComponents(fakeid, ComponentOne)
            .delete(fakeid);
        const gen = commands[Symbol.iterator]();
        const first = gen.next().value;
        const second = gen.next().value;
        const third = gen.next().value;
        const fourth = gen.next().value;
        expect(first?.type).toBe(CommandType.CREATE_ENTITY);
        expect(first?.components.length).toBe(2);
        const firstComponentOne = first?.components[0](resources);
        const firstComponentTwo = first?.components[1](resources);
        expect(firstComponentOne.__id).toBe(0);
        expect(firstComponentOne.one).toBe(true);
        expect(firstComponentTwo.__id).toBe(1);
        expect(firstComponentTwo.two).toBe(true);
        expect(second?.type).toBe(CommandType.ADD_COMPONENTS);
        expect(second?.components.length).toBe(1);
        const secondComponentTwo = second?.components[0](resources);
        expect(secondComponentTwo.__id).toBe(1);
        expect(secondComponentTwo.two).toBe(true);
        expect(third).toStrictEqual({
            type: CommandType.REMOVE_COMPONENTS,
            entityId: fakeid,
            components:[ComponentOne],
        });
        expect(fourth).toStrictEqual({
            type: CommandType.DELETE_ENTITY,
            entityId: fakeid,
        });
    });
});
