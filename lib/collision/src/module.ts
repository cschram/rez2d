import { Module, SystemPriority } from "@rez2d/ecs";
import { QuadTree } from "./quadtree";
import { Rect } from "./rect";
import { CollisionConfig, CollisionResources, QuadTreeData } from "./types";
import { PopulateTree, MoveColliders } from "./systems";
import { RenderResources } from "@rez2d/render";

export function CollisionModule<TResources extends CollisionResources & RenderResources>({ worldSize: size }: CollisionConfig): Module<TResources> {
    return (_store, systems, resources) => {
        resources.set("collisionTree", new QuadTree<QuadTreeData>(new Rect(0, 0, size.width, size.height)));
        systems
            .add(PopulateTree, SystemPriority.HIGH)
            .add(MoveColliders, SystemPriority.HIGH);
    };
}
