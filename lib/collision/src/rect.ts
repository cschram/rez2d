import { Vec } from "ella-math";
import { Polygon } from "./polygon";

export class Rect {
    constructor(
        public x: number,
        public y: number,
        public w: number,
        public h: number,
    ) {}

    static container(rects: Rect[]): Rect {
        if (rects.length === 0) {
            throw new Error("Cannot create a container rect for 0 rects");
        }
        if (rects.length === 1) {
            return rects[0];
        }
        let left = rects[0].left;
        let right = rects[0].right;
        let top = rects[0].top;
        let bottom = rects[0].bottom;
        for (let i = 1; i < rects.length; i++) {
            if (rects[i].left < left) {
                left = rects[i].left;
            } else if (rects[i].right > right) {
                right = rects[i].right;
            }
            if (rects[i].top < top) {
                top = rects[i].top;
            } else if (rects[i].bottom > bottom) {
                bottom = rects[i].bottom;
            }
        }
        const w = right - left;
        const h = bottom - top;
        return new Rect(
            left + (w / 2),
            top + (h / 2),
            w,
            h,
        );
    }

    get left(): number {
        return this.x - (this.w / 2);
    }

    set left(x: number) {
        this.x = x + (this.w / 2);
    }

    get right(): number {
        return this.x + (this.w / 2);
    }

    set right(x: number) {
        this.x = x - (this.w / 2);
    }

    get top(): number {
        return this.y - (this.h / 2);
    }

    set top(y: number) {
        this.y = y + (this.h / 2);
    }

    get bottom(): number {
        return this.y + (this.h / 2);
    }

    set bottom(y: number) {
        this.y = y - (this.h / 2);
    }

    get center(): Vec {
        return new Vec(this.x, this.y);
    }

    set center(c: Vec) {
        this.x = c.x;
        this.y = c.y;
    }

    clone(): Rect {
        return new Rect(this.x, this.y, this.w, this.h);
    }

    translate(x: number, y: number): Rect {
        return new Rect(this.x + x, this.y + y, this.w, this.h);
    }

    intersects(other: Rect): boolean {
        const left = Math.max(this.left, other.left);
        const top = Math.max(this.top, other.top);
        const right = Math.min(this.right, other.right);
        const bottom = Math.min(this.bottom, other.bottom);
        return left < right && top < bottom;
    }

    toPolygon(): Polygon {
        return Polygon.from(this);
    }
}
