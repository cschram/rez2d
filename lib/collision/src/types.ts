import { EntityId } from "@rez2d/ecs";
import { QuadTree } from "./quadtree";
import { CollisionInfo } from "./collision";
import { Body } from "./body";

export interface CollisionConfig {
    worldSize: {
        width: number;
        height: number;
    };
}

export type QuadTreeData = [EntityId, Body];

export interface CollisionResources {
    collisionTree: QuadTree<QuadTreeData>;
}

export interface Collision extends CollisionInfo {
    collider: [EntityId, Body];
    blocker: [EntityId, Body];
}
