import { Vec } from "ella-math";
import { Polygon } from "./polygon";
import { Rect } from "./rect";

export interface Shape {
    center: Vec;
    scale: Vec;
    rotation: number;
    bounds: Rect;
    isPolygon(): this is Polygon;
}
