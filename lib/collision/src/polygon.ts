import { Vec, Mat2 } from "ella-math";
import { Rect } from "./rect";
import { Shape } from "./shape";
import { normal } from "./util";

export interface PolygonEdge {
    start: Vec;
    end: Vec;
}

export interface PolygonProjection {
    min: number;
    max: number;
}

export class Polygon implements Shape {
    private _center: Vec;
    private _rot: number;
    private _scale: Vec;
    private _verts: Vec[];
    private _tVerts: Vec[];
    private _edges: PolygonEdge[];
    private _normals: Vec[];
    private _bounds: Rect;

    constructor(pos?: Vec, vertices?: Vec[]) {
        this._center = pos || new Vec(0, 0);
        this._rot = 0;
        this._scale = new Vec(1, 1);
        this._verts = vertices || [];
        this._tVerts = [];
        this._edges = [];
        this._normals = [];
        this._bounds = new Rect(0, 0, 0, 0);
        this.recalculate();
    }

    static from(other: Rect): Polygon {
        return Polygon.box(other.x, other.y, other.w, other.h);
    }

    static box(x: number, y: number, w: number, h: number): Polygon {
        const hw = w / 2;
        const hh = h / 2;
        return new Polygon(
            new Vec(x, y),
            [
                // Top left
                new Vec(-hw, -hh),
                // Top right
                new Vec(hw, -hh),
                // Bottom right
                new Vec(hw, hh),
                // Bottom left
                new Vec(-hw, hh),
            ],
        );
    }

    static circle(center: Vec, radius: number, numVertices: number = 16): Polygon {
        const vertices: Vec[] = [];
        const step = (Math.PI * 2) / numVertices;
        for (let i = 0; i < numVertices; i++) {
            const angle = (Math.PI * 2) - (step * i);
            vertices.push(
                new Vec(
                    radius + (Math.sin(angle) * radius),
                    radius + (Math.cos(angle) * radius),
                )
            );
        }
        return new Polygon(center, vertices);
    }

    get center(): Vec {
        return this._center;
    }

    set center(pos: Vec) {
        this._center = pos;
        this.recalculate();
    }

    get rotation(): number {
        return this._rot;
    }

    set rotation(rot: number) {
        this._rot = rot
        this.recalculate();
    }

    get scale(): Vec {
        return this._scale;
    }

    set scale(scale: Vec) {
        this._scale = scale;
        this.recalculate();
    }

    get vertices(): Vec[] {
        return this._verts;
    }

    set vertices(verts: Vec[]) {
        this._verts = verts;
        this.recalculate();
    }

    get translatedVertices(): Vec[] {
        return this._tVerts;
    }

    get edges(): PolygonEdge[] {
        return this._edges;
    }

    get normals(): Vec[] {
        return this._normals;
    }

    get bounds(): Rect {
        return this._bounds;
    }

    isPolygon(): this is Polygon {
        return true;
    }

    project(axis: Vec): PolygonProjection {
        const projection = {
            min: Infinity,
            max: -Infinity,
        };
        this._tVerts.forEach((vert) => {
            const dot = axis.dot(vert);
            if (dot < projection.min) {
                projection.min = dot;
            } else if (dot > projection.max) {
                projection.max = dot;
            }
        });
        return projection;
    }

    private recalculate() {
        // @ts-expect-error
        this._tVerts = this._verts.map((vert) => {
            return Mat2.rotation(this._rot)
                .mul(Mat2.scaling(this._scale.x, this._scale.y))
                // @ts-expect-error
                .mul(vert.add(this._center));
        });
        this._edges = [];
        for (let i = 0; i < this._tVerts.length; i++) {
            const start = this._tVerts[i];
            const end = (i + 1) === this._tVerts.length
                ? this._tVerts[0]
                : this._tVerts[i + 1];
            this._edges.push({ start, end });
        }
        this._normals = this._edges.map(({ start, end }) => {
            return normal(end.sub(start));
        });
        let left = Number.MAX_VALUE;
        let right = -Number.MAX_VALUE;
        let top = Number.MAX_VALUE;
        let bottom = -Number.MAX_VALUE;
        this._tVerts.forEach((vert) => {
            if (vert.x < left) {
                left = vert.x;
            } else if (vert.x > right) {
                right = vert.x;
            }
            if (vert.y < top) {
                top = vert.y;
            } else if (vert.y > bottom) {
                bottom = vert.y;
            }
        });
        const w = right - left;
        const h = bottom - top;
        this._bounds = new Rect(left + (w / 2), top + (h / 2), w, h);
    }
}
