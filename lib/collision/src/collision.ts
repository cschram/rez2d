import { Vec } from "ella-math";
import { Option, Some, None } from "oxide.ts";
import { clamp } from "./util";
import { Polygon, PolygonProjection } from "./polygon";
import { Shape } from "./shape";

export interface CollisionInfo {
    // The distance in the movement that the collision occurs,
    // on a scale from 0 to 1
    readonly distance: number;
    // Normal vector of the collision
    readonly normal: Vec;
    // Translation vector to prevent collision
    readonly translation: Vec;
}

function concatNormals(a: Polygon, b: Polygon): Vec[] {
    const unique: Vec[] = [];
    const uniqueStrs: string[] = [];
    a.normals.concat(b.normals).forEach((vec) => {
        const str = `${vec.x},${vec.y}`;
        if (uniqueStrs.indexOf(str) === -1) {
            unique.push(vec);
            uniqueStrs.push(str);
        }
    });
    return unique;
}

function projectionDistance(a: PolygonProjection, b: PolygonProjection): number {
    if (a.min < b.min) {
        return b.min - a.max;
    } else {
        return a.min - b.max;
    }
}

function collidePolyPoly(collider: Polygon, movement: Vec, blocker: Polygon): Option<CollisionInfo> {
    const scale = new Vec(
        movement.x === 0 ? 0 : 1 / movement.x,
        movement.y === 0 ? 0 : 1 / movement.y,
    ).length;
    let intersects = true;
    let willIntersect = true;
    let collisionDistance = Infinity;
    let collisionNormal = new Vec(0, 0);
    const normals = concatNormals(collider, blocker);
    for (let i = 0; i < normals.length; i++) {
        const axis = normals[i];

        // Check if there is currently a collision
        const cProj = collider.project(axis);
        const bProj = blocker.project(axis);
        if (projectionDistance(cProj, bProj) > 0) {
            intersects = false;
        }

        // Check if there's a collision after applying movement
        const mDot = axis.dot(movement);
        const mProj = mDot < 0
            ? { min: cProj.min + mDot, max: cProj.max }
            : { min: cProj.min, max: cProj.max + mDot };
        const mDist = projectionDistance(mProj, bProj);
        if (mDist > 0) {
            willIntersect = false;
        }

        if (!intersects && !willIntersect) {
            return None;
        }

        // Keep track of this distance and normal if it's the closest collision
        const distance = Math.abs(mDist);
        if (distance < collisionDistance) {
            collisionDistance = distance;
            const d = blocker.center.sub(collider.center);
            if (d.dot(axis) < 0) {
                collisionNormal = new Vec(
                    -axis.x,
                    -axis.y,
                );
            } else {
                collisionNormal = axis;
            }
        }
    }
    if (collisionNormal.length > 0) {
        return Some({
            distance: 1 - clamp(collisionDistance * scale, 0, 1),
            normal: collisionNormal,
            translation: collisionNormal.mul(collisionDistance)
        });
    } else {
        return None;
    }
}

export function collide(collider: Shape, velocity: Vec, blocker: Shape): Option<CollisionInfo> {
    if (collider.isPolygon() && blocker.isPolygon()) {
        return collidePolyPoly(collider, velocity, blocker);
    } else {
        return None;
    }
}
