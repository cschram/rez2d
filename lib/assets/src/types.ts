import { Option, Result } from "oxide.ts";

export interface AssetManagerConfig {
    baseUrl: string;
}

export enum AssetState {
    LOADING,
    LOADED,
    ERROR,
}

export interface AssetHandle<TAsset> {
    state: AssetState;
    asset: Option<TAsset>;
    error?: string;
}

export interface AssetLoadFailure {
    filename: string;
    error: string;
}

export type AssetLoader<TAsset> = (path: string, config: AssetManagerConfig) => Promise<TAsset>;
