export class IDGenerator {
    private _lastId: number;

    constructor() {
        this._lastId = 0;
    }

    get(): number {
        return this._lastId++;
    }

    reset() {
        this._lastId = 0;
    }
}