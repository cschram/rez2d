import { Vec } from "ella-math";
import { createComponent } from "@rez2d/ecs";

interface TransformArgs {
    pos?: [number, number] | Vec;
    scale?: [number, number] | Vec;
    rotation?: number;
}

export const Transform = createComponent((_resources, args: TransformArgs) => {
    let pos: Vec = new Vec(0, 0);
    let scale: Vec = new Vec(1, 1);
    if (Array.isArray(args.pos)) {
        pos = new Vec(args.pos[0], args.pos[1]);
    } else if (args.pos instanceof Vec) {
        pos = args.pos;
    }
    if (Array.isArray(args.scale)) {
        scale = new Vec(args.scale[0], args.scale[1]);
    } else if (args.scale instanceof Vec) {
        scale = args.scale;
    }
    return {
        pos,
        scale,
        rotation: args?.rotation || 0,
    };
});
