import { Option, Some, None, match } from "oxide.ts";
import { SlotKey, Slot } from "./types";

export class SlotMap<TValue> {
    private slots: Slot<TValue>[];
    private free: number[];

    constructor() {
        this.slots = [];
        this.free = [];
    }

    contains(key: SlotKey): boolean {
        return key.index < this.slots.length &&
            this.slots[key.index].version === key.version;
    }

    get(key: SlotKey): Option<TValue> {
        if (this.contains(key)) {
            return this.slots[key.index].value;
        } else {
            return None;
        }
    }

    insert(value: TValue): SlotKey {
        return match(this.getFree(), {
            Some: (key) => {
                this.slots[key.index].value = Some(value);
                return key;
            },
            None: () => {
                this.slots.push({
                    value: Some(value),
                    version: 1,
                });
                return {
                    index: this.slots.length - 1,
                    version: 1,
                };
            },
        });
    }

    remove(key: SlotKey) {
        if (this.contains(key)) {
            this.free.push(key.index);
            this.slots[key.index] = {
                value: None,
                version: key.version + 1,
            };
        }
    }

    clear() {
        this.free = [];
        for (let i = 0; i < this.slots.length; i++) {
            this.slots[i] = {
                value: None,
                version: this.slots[i].version + 1,
            };
            this.free.push(i);
        }
    }

    *[Symbol.iterator](): IterableIterator<[SlotKey, TValue]> {
        const slots = this.slots.slice();
        for (let i = 0; i < slots.length; i++) {
            if (slots[i].value.isSome()) {
                yield [
                    {
                        index: i,
                        version: slots[i].version,
                    },
                    slots[i].value.unwrap()
                ];
            }
        }
    }

    private getFree(): Option<SlotKey> {
        const index = this.free.pop();
        if (index == undefined) {
            return None;
        } else {
            return Some({
                index,
                version: this.slots[index].version,
            });
        }
    }
}
