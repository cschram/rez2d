import { SlotMap } from "./slotmap";
import { SecondaryMap } from "./secondary";

describe("SecondaryMap", () => {
    it("should insert slots with key from SlotMap", () => {
        const primary = new SlotMap<string>();
        const secondary = new SecondaryMap<number>();
        const keyOne = primary.insert("foo");
        const keyTwo = primary.insert("bar");
        secondary.insert(keyOne, 42);
        expect(secondary.get(keyOne).unwrap()).toBe(42);
        expect(secondary.get(keyTwo).isNone()).toBe(true);
    });

    it("should not insert a lot when a newer version exists", () => {
        const primary = new SlotMap<string>();
        const secondary = new SecondaryMap<number>();
        const key = primary.insert("foo");
        secondary.insert(key, 42);
        secondary.remove(key);
        secondary.insert(key, 42);
        expect(secondary.get(key).isNone()).toBe(true);
    });

    it("should clear the map", () => {
        const primary = new SlotMap<string>();
        const secondary = new SecondaryMap<number>();
        const keyOne = primary.insert("foo");
        const keyTwo = primary.insert("bar");
        secondary.insert(keyOne, 42);
        secondary.insert(keyTwo, 43);
        secondary.clear();
        expect(secondary.get(keyOne).isNone()).toBe(true);
        expect(secondary.get(keyTwo).isNone()).toBe(true);
    });

    it("should iterate over values", () => {
        const fn = jest.fn();
        const primary = new SlotMap<string>();
        const secondary = new SecondaryMap<number>();
        const keyOne = primary.insert("foo");
        const keyTwo = primary.insert("bar");
        const keyThree = primary.insert("baz");
        secondary.insert(keyOne, 42);
        secondary.insert(keyTwo, 43);
        secondary.insert(keyThree, 44);
        primary.remove(keyTwo);
        secondary.remove(keyTwo);
        for (const value of secondary) {
            fn(value);
        }
        expect(fn).toHaveBeenCalledTimes(2);
        expect(fn).toHaveBeenCalledWith([
            { index: 0, version: 1 },
            42,
        ]);
        expect(fn).toHaveBeenCalledWith([
            { index: 2, version: 1 },
            44,
        ]);
    });
});
