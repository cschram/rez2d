import { Option, Some, None, match } from "oxide.ts";
import { SlotKey, Slot } from "./types";

export class SecondaryMap<TValue> {
    private slots: Slot<TValue>[];

    constructor() {
        this.slots = [];
    }

    contains(key: SlotKey): boolean {
        return key.index < this.slots.length &&
            this.slots[key.index].version === key.version;
    }

    get(key: SlotKey): Option<TValue> {
        if (this.contains(key)) {
            return this.slots[key.index].value;
        } else {
            return None;
        }
    }

    insert(key: SlotKey, value: TValue) {
        this.reserve(key.index + 1 - this.slots.length);
        if (this.slots[key.index].version <= key.version) {
            this.slots[key.index] = {
                value: Some(value),
                version: key.version,
            };
        }
    }

    remove(key: SlotKey) {
        if (this.contains(key)) {
            this.slots[key.index] = {
                value: None,
                version: key.version + 1,
            };
        }
    }

    clear() {
        for (let i = 0; i < this.slots.length; i++) {
            this.slots[i] = {
                value: None,
                version: this.slots[i].version + 1,
            };
        }
    }

    *[Symbol.iterator](): IterableIterator<[SlotKey, TValue]> {
        const slots = this.slots.slice();
        for (let i = 0; i < slots.length; i++) {
            if (slots[i].value.isSome()) {
                yield [
                    {
                        index: i,
                        version: slots[i].version,
                    },
                    slots[i].value.unwrap()
                ];
            }
        }
    }

    private reserve(additional: number) {
        if (additional > 0) {
            for (let i = 0; i < additional; i++) {
                this.slots.push({
                    value: None,
                    version: 1,
                });
            }
        }
    }
}
