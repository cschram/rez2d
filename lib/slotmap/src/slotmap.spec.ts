import { SlotMap } from "./slotmap";

describe("SlotMap", () => {
    it("should insert slots", () => {
        const map = new SlotMap<string>();
        const key = map.insert("foo");
        expect(key).toStrictEqual({
            index: 0,
            version: 1,
        });
        expect(map.contains(key)).toBe(true);
        expect(map.get(key).unwrap()).toBe("foo");
    });

    it("should remove slots", () => {
        const map = new SlotMap<string>();
        const key = map.insert("foo");
        map.remove(key);
        expect(map.get(key).isNone()).toBe(true);
    });

    it("should reuse removed slots", () => {
        const map = new SlotMap<string>();
        const origKey = map.insert("foo");
        map.remove(origKey);
        const newKey = map.insert("bar");
        expect(newKey).toStrictEqual({
            index: 0,
            version: 2,
        });
        expect(map.get(newKey).unwrap()).toBe("bar");
    });

    it("should clear the map", () => {
        const map = new SlotMap<string>();
        const keyOne = map.insert("foo");
        const keyTwo = map.insert("bar");
        map.clear();
        expect(map.get(keyOne).isNone()).toBe(true);
        expect(map.get(keyTwo).isNone()).toBe(true);
    });

    it("should iterate over values", () => {
        const fn = jest.fn();
        const map = new SlotMap<string>();
        map.insert("foo");
        const key = map.insert("bar");
        map.insert("baz");
        map.remove(key);
        for (const value of map) {
            fn(value);
        }
        expect(fn).toHaveBeenCalledTimes(2);
        expect(fn).toHaveBeenCalledWith([
            { index: 0, version: 1 },
            "foo",
        ]);
        expect(fn).toHaveBeenCalledWith([
            { index: 2, version: 1 },
            "baz",
        ]);
    });
});
