
import z from "zod";

export const SpritesheetSpriteSchema = z.object({
    name: z.string(),
    center: z.tuple([z.number().int(), z.number().int()]),
    size: z.tuple([z.number().int(), z.number().int()])
});

export type TSpritesheetSpriteSchema = z.infer<typeof SpritesheetSpriteSchema>;

export const SpritesheetAnimationSchema = z.object({
    name: z.string(),
    frames: z.array(z.object({
        sprite: z.string(),
        length: z.number().int(),
    })),
});

export type TSpritesheetAnimationSchema = z.infer<typeof SpritesheetAnimationSchema>;

export const SpritesheetSchema = z.object({
    source: z.string(),
    sprites: z.array(SpritesheetSpriteSchema),
    animations: z.array(SpritesheetAnimationSchema),
});

export type TSpritesheetSchema = z.infer<typeof SpritesheetSchema>;
