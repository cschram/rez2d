import { Vec } from "ella-math";

export class Camera {
    constructor(
        public pos: Vec = new Vec(0, 0)
    ) {}
}
