import { AssetsResources } from "@rez2d/assets";
import { Renderer } from "./renderer";

export interface RendererConfig {
    container: HTMLCanvasElement;
    width: number;
    height: number;
}

export type RenderResources = AssetsResources & {
    dt: number;
    renderer: Renderer;
}
