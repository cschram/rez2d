import * as PIXI from "pixi.js";
import { AssetManagerConfig } from "@rez2d/assets";

export function loadTexture(path: string, _config: AssetManagerConfig): Promise<PIXI.BaseTexture> {
    return new Promise((resolve, reject) => {
        const img = new Image();
        img.src = path;
        img.onload = () => {
            resolve(new PIXI.BaseTexture(img));
        };
        img.onerror = () => {
            reject(`Unable to load texture "${path}"`);
        };
    });
}
