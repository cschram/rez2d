import { AssetManagerConfig } from "@rez2d/assets";
import { loadTexture } from "./texture";
import { SpritesheetSchema } from "../schema";
import { SpritesheetContainer } from "../spritesheet";

export async function loadSpritesheet(path: string, config: AssetManagerConfig): Promise<SpritesheetContainer> {
    const response = await fetch(path);
    const rawData = await response.json();
    const data = SpritesheetSchema.parse(rawData);
    const baseTexture = await loadTexture(`${config.baseUrl}/${data.source}`, config);
    return new SpritesheetContainer(baseTexture, data);
}
