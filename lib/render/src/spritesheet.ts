import * as PIXI from "pixi.js";
import { Option, Some, None } from "oxide.ts";
import {
    TSpritesheetSpriteSchema,
    TSpritesheetAnimationSchema,
    TSpritesheetSchema,
} from "./schema";

/**
 * Spritesheet wrapper container.
 * Pixi.js's Spritesheet implementation does not support frame lengths for animations,
 * so it's necessary to have this custom implementation.
 */
export class SpritesheetContainer {
    private _texture: PIXI.BaseTexture;
    private _sprites: Record<string, PIXI.Texture>;
    private _animations: Record<string, PIXI.FrameObject[]>;
    private _data: TSpritesheetSchema;

    constructor(texture: PIXI.BaseTexture, data: TSpritesheetSchema) {
        this._texture = texture;
        this._sprites = {};
        this._animations = {};
        this._data = {
            ...data,
            sprites: [],
            animations: [],
        };
        for (const sprite of data.sprites) {
            this.setSprite(sprite);
        }
        for (const animation of data.animations) {
            this.setAnimation(animation);
        }
    }

    getSprite(name: string): Option<PIXI.Sprite> {
        if (this._sprites.hasOwnProperty(name)) {
            return Some(new PIXI.Sprite(this._sprites[name]));
        } else {
            return None;
        }
    }

    setSprite(sprite: TSpritesheetSpriteSchema) {
        if (this._sprites.hasOwnProperty(sprite.name)) {
            this._data.sprites = this._data.sprites
                .map((s) => {
                    if (s.name === sprite.name) {
                        return sprite;
                    } else {
                        return s;
                    }
                });
            this._sprites[sprite.name].frame = new PIXI.Rectangle(
                sprite.center[0] - (sprite.size[0] / 2),
                sprite.center[1] - (sprite.size[1] / 2),
                sprite.size[0],
                sprite.size[1],
            );
            this._sprites[sprite.name].defaultAnchor = new PIXI.Point(sprite.center[0], sprite.center[1]);
        } else {
            this._data.sprites.push(sprite);
            this._sprites[sprite.name] = new PIXI.Texture(
                this._texture,
                new PIXI.Rectangle(
                    sprite.center[0] - (sprite.size[0] / 2),
                    sprite.center[1] - (sprite.size[1] / 2),
                    sprite.size[0],
                    sprite.size[1],
                ),
                new PIXI.Rectangle(this._texture.realWidth, this._texture.realHeight),
                undefined,
                undefined,
                {
                    x: sprite.center[0],
                    y: sprite.center[1],
                },
            );
        }
    }

    deleteSprite(name: string) {
        this._data.sprites = this._data.sprites
            .filter((sprite) => sprite.name !== name);
        delete this._sprites[name];
    }

    getAnimatedSprite(name: string): Option<PIXI.AnimatedSprite> {
        if (this._animations.hasOwnProperty(name)) {
            return Some(new PIXI.AnimatedSprite(this._animations[name]));
        } else {
            return None;
        }
    }

    setAnimation(animation: TSpritesheetAnimationSchema) {
        if (this._animations.hasOwnProperty(animation.name)) {
            this._data.animations = this._data.animations
                .map((anim) => {
                    if (anim.name === animation.name) {
                        return animation;
                    } else {
                        return anim;
                    }
                });

        } else {
            this._data.animations.push(animation);
        }
        this._animations[animation.name] = animation.frames.map((frame) => {
            return {
                texture: this._sprites[frame.sprite],
                time: frame.length,
            };
        });
    }

    deleteAnimation(name: string) {
        this._data.animations = this._data.animations
            .filter((animation) => animation.name !== name);
        delete this._animations[name];
    }

    toJSON(): string {
        return JSON.stringify(this._data);
    }
}
