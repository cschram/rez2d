import { Transform } from "@rez2d/common";
import {
    EntityStore,
    Query,
    Resources,
} from "@rez2d/ecs";
import { Sprite, Spritesheet, AnimatedSprite, Label } from "./components";
import { RenderResources } from "./types";

export function SpriteTransform<TResources extends RenderResources>(
    store: EntityStore<TResources>,
    resources: Resources<TResources>,
) {
    const dt = resources.get("dt").unwrap();
    const renderer = resources.get("renderer").unwrap();
    renderer.update(dt);
    [Sprite, Spritesheet, AnimatedSprite, Label].forEach((Drawable) => {
        for (const [_, { container }, { pos, scale, rotation }] of store.query(new Query(Drawable, Transform))) {
            container.position.set(pos.x, pos.y);
            container.scale.set(scale.x, scale.y);
            container.angle = rotation;
        }
    });
};

export function ClearRendererEvents<TResources extends RenderResources>(
    _store: EntityStore<TResources>,
    resources: Resources<TResources>,
) {
    const renderer = resources.get("renderer").unwrap();
    renderer.events.empty();
}
