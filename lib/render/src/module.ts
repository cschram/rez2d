import { Module, SystemPriority } from "@rez2d/ecs";
import { AssetsResources } from "@rez2d/assets";
import { Renderer } from "./renderer";
import { SpriteTransform, ClearRendererEvents } from "./systems";
import { RendererConfig, RenderResources } from "./types";
import { loadTexture, loadSpritesheet } from "./loaders";

export function RenderModule<TResources extends RenderResources>(config: RendererConfig): Module<TResources> {
    return (store, systems, resources) => {
        // Setup pixi application
        const renderer = new Renderer(config);
        resources.set("renderer", renderer);

        // Call schedule every tick
        renderer.onTick((dt) => {
            resources.set("dt", dt);
            systems.run(store, resources);
        });

        // Setup systems
        systems
            .add(SpriteTransform, SystemPriority.HIGH)
            .add(ClearRendererEvents, SystemPriority.LOW);

        // Register asset loaders
        const assets = resources.get("assets").unwrap();
        assets.registerLoader(["png", "gif", "jpg"], loadTexture);
        assets.registerLoader(["r2s"], loadSpritesheet);
    };
};
