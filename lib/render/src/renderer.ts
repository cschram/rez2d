import * as PIXI from "pixi.js";
import { Option, Some, None } from "oxide.ts";
import { Camera } from "./camera";
import { RendererConfig } from "./types";
import { EventQueue } from "@rez2d/events";

interface LoadCompleteEvent {
    resource: PIXI.LoaderResource;
}

interface LoadErrorEvent {
    error: Error;
    resource: PIXI.LoaderResource;
}

type RendererEvents = {
    all_complete: void;
    complete: LoadCompleteEvent;
    error: LoadErrorEvent;
};

export class Renderer {
    private _pixi: PIXI.Application;
    private _loader: PIXI.Loader;
    private _camera: Camera;
    private _events: EventQueue<RendererEvents>;

    constructor(config: RendererConfig) {
        this._pixi = new PIXI.Application({
            view: config.container,
            width: config.width,
            height: config.height,
        });
        this._pixi.stage.position.set(
            (config.width / 2),
            (config.height / 2),
        );
        this._pixi.stage.sortableChildren = true;
        document.body.appendChild(this._pixi.view);
        this._loader = new PIXI.Loader();
        this._camera = new Camera();
        this._events = new EventQueue();
        this._loader.onComplete.add((_, resources) => {
            for (const resource of Object.values(resources)) {
                this._events.publish("complete", {
                    resource,
                });
            }
        });
        this._loader.onError.add((error, _, resource) => {
            this._events.publish("error", {
                error,
                resource,
            });
        });
    }

    get camera(): Camera {
        return this._camera;
    }

    get events(): EventQueue<RendererEvents> {
        return this._events;
    }

    onTick(fn: (dt: number) => void) {
        this._pixi.ticker.add(fn);
    }

    addChild(...container: PIXI.Container[]): this {
        this._pixi.stage.addChild(...container);
        return this;
    }

    removeChild(...container: PIXI.Container[]): this {
        this._pixi.stage.removeChild(...container);
        return this;
    }

    update(_dt: number) {
        this._pixi.stage.position.set(
            -this._camera.pos.x + (this._pixi.view.width / 2),
            -this._camera.pos.y + (this._pixi.view.height / 2),
        );
    }
}
