import * as PIXI from "pixi.js";
import { createComponent } from "@rez2d/ecs";
import { DrawableContainer } from "./drawable";
import { SpritesheetContainer } from "../spritesheet";
import { RenderResources } from "../types";

interface SpritesheetArgs {
    src: string;
    defaultSprite: string;
    zIndex?: number;
}

interface SpritesheetData extends DrawableContainer {
    readonly spritesheet: SpritesheetContainer;
    readonly sprite: PIXI.Sprite;
    setSprite(name: string): void;
}

export const Spritesheet = createComponent<SpritesheetData, SpritesheetArgs, RenderResources>((resources, args) => {
    const assets = resources.get("assets").unwrap();
    const renderer = resources.get("renderer").unwrap();
    const spritesheet = assets.get<SpritesheetContainer>(args.src)
        .expect(`No spritesheet "${args.src}" loaded`);
    let sprite = spritesheet.getSprite(args.defaultSprite)
        .expect(`No sprite "${args.defaultSprite}" in sheet "${args.src}"`);
    sprite.anchor.set(0.5, 0.5);
    if (args.zIndex) {
        sprite.zIndex = args.zIndex;
    }
    renderer.addChild(sprite);
    return {
        spritesheet,
        get sprite() {
            return sprite;
        },
        get container() {
            return sprite;
        },
        setSprite(name: string) {
            renderer.removeChild(sprite);
            sprite = spritesheet.getSprite(name)
                .expect(`No sprite "${name}" in sheet "${args.src}"`);
            sprite.anchor.set(0.5, 0.5);
            if (args.zIndex) {
                sprite.zIndex = args.zIndex;
            }
            renderer.addChild(sprite);
        },
    };
}, (resources, { sprite }) => {
    const renderer = resources.get("renderer").unwrap();
    renderer.removeChild(sprite);
});
