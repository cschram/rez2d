import * as PIXI from "pixi.js";
import { createComponent } from "@rez2d/ecs";
import { DrawableContainer } from "./drawable";
import { RenderResources } from "../types";

interface LabelArgs {
    text: string;
    style?: Partial<PIXI.ITextStyle>;
    zIndex?: number;
}

interface LabelData extends DrawableContainer {
    readonly text: PIXI.Text;
    readonly metrics: PIXI.TextMetrics;
    setStyle(args: Partial<PIXI.ITextStyle>): void;
    setText(str: string): void;
}

export const Label = createComponent<LabelData, LabelArgs, RenderResources>((resources, args) => {
    const renderer = resources.get("renderer").unwrap();
    const initialStyle = new PIXI.TextStyle(args.style);

    let text = new PIXI.Text(args.text, initialStyle);
    let metrics = PIXI.TextMetrics.measureText(args.text, initialStyle);
    if (args.zIndex) {
        text.zIndex = args.zIndex;
    }
    renderer.addChild(text);
    return {
        get text() {
            return text;
        },
        get container() {
            return text;
        },
        get metrics() {
            return metrics;
        },
        setStyle(style: Partial<PIXI.ITextStyle>) {
            text.style = style;
            metrics = PIXI.TextMetrics.measureText(text.text, new PIXI.TextStyle(text.style));
        },
        setText(str: string) {
            text.text = str;
            metrics = PIXI.TextMetrics.measureText(text.text, new PIXI.TextStyle(text.style));
        },
    };
}, (resources, { text }) => {
    const renderer = resources.get("renderer").unwrap();
    renderer.removeChild(text);
});
