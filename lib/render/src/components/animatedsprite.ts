import * as PIXI from "pixi.js";
import { createComponent } from "@rez2d/ecs";
import { DrawableContainer } from "./drawable";
import { SpritesheetContainer } from "../spritesheet";
import { RenderResources } from "../types";

interface AnimatedSpriteArgs {
    src: string;
    name: string;
    loop?: boolean;
    zIndex?: number;
}

interface AnimatedSpriteData extends DrawableContainer {
    readonly spritesheet: SpritesheetContainer;
    readonly sprite: PIXI.AnimatedSprite;
    play(name: string, speed?: number, loop?: boolean): void;
}

export const AnimatedSprite = createComponent<AnimatedSpriteData, AnimatedSpriteArgs, RenderResources>((resources, args) => {
    const assets = resources.get("assets").unwrap();
    const renderer = resources.get("renderer").unwrap();
    const spritesheet = assets.get<SpritesheetContainer>(args.src)
        .expect(`No spritesheet "${args.src}" loaded`);
    let sprite = spritesheet.getAnimatedSprite(args.name)
        .expect(`No animation "${args.name}" in spritesheet "${args.src}"`);
    sprite.anchor.set(0.5, 0.5);
    if (args.zIndex) {
        sprite.zIndex = args.zIndex;
    }
    sprite.loop = args.loop ?? true;
    sprite.play();
    renderer.addChild(sprite);
    return {
        spritesheet,
        get sprite() {
            return sprite;
        },
        get container() {
            return sprite;
        },
        play(name: string, speed = 1, loop = true) {
            renderer.removeChild(sprite);
            sprite = spritesheet.getAnimatedSprite(name)
                .expect(`No animation "${name}" in spritesheet "${args.src}"`);
            sprite.anchor.set(0.5, 0.5);
            if (args.zIndex) {
                sprite.zIndex = args.zIndex;
            }
            sprite.loop = loop;
            sprite.play();
            renderer.addChild(sprite);
        },
    }
}, (resources, { sprite }) => {
    const renderer = resources.get("renderer").unwrap();
    renderer.removeChild(sprite);
});
