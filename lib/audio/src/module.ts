import { Module, SystemPriority } from "@rez2d/ecs";
import { AudioPlayer } from "./player";
import { UpdateAudio, ClearAudioEvents } from "./systems";
import { AudioResources } from "./types";

export function AudioModule<TResources extends AudioResources>(): Module<TResources> {
    return (_store, schedule, resources) => {
        resources.set("audio", new AudioPlayer());
        schedule
            .add(UpdateAudio, SystemPriority.HIGH)
            .add(ClearAudioEvents, SystemPriority.LOW);
    };
}
