import { createComponent } from "@rez2d/ecs";
import { Sound, SoundOptions } from "./sound";

export interface AudioSourceData {
    sound: Sound;
}

export interface AudioSourceArgs {
    src: string;
    options?: SoundOptions;
}

export const AudioSource = createComponent<AudioSourceData, AudioSourceArgs>((_, args) => {
    return {
        sound: new Sound(args.src, args.options),
    };
});

export interface AudioListenerData {
    volume: number;
}

export interface AudioListenerArgs {
    volume?: number;
}

export const AudioListener = createComponent<AudioListenerData, AudioListenerArgs>((_, args) => {
    return {
        volume: args.volume || 1,
    };
});
