import { match } from "oxide.ts";
import { Transform } from "@rez2d/common";
import { EntityStore, Query, Resources } from "@rez2d/ecs";
import { AudioSource, AudioListener } from "./components";
import { AudioResources } from "./types";

export function UpdateAudio<TResources extends AudioResources>(
    store: EntityStore<TResources>,
    resources: Resources<TResources>,
) {
    const player = resources.get("audio").unwrap();
    player.update();
    match(store.queryFirst(new Query(Transform, AudioListener)), {
        Some: ([_, transform, listener]) => {
            player.pos = transform.pos;
            player.volume = listener.volume;
        },
        None: () => {},
    });
    for (const [_, transform, source] of store.query(new Query(Transform, AudioSource))) {
        source.sound.pos = transform.pos;
    }
};

export function ClearAudioEvents<TResources extends AudioResources>(
    store: EntityStore<TResources>,
    resources: Resources<TResources>,
) {
    const player = resources.get("audio").unwrap();
    player.clearEvents();
    for (const [_, source] of store.query(new Query(AudioSource))) {
        source.sound.events.empty();
    }
}
