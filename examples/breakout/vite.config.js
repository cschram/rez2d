/**
 * @type {import('vite').UserConfig}
 */
 const config = {
    assetsInclude: ["**/*.json"],
};

export default config;
