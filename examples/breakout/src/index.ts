import { Vec } from "ella-math";
import { match } from "oxide.ts";
import {
    Rez,
    Query,
    Transform,
    Resources,
    RezResources,
    EntityStore,
    Collider,
    Polygon,
    Sprite,
    Spritesheet,
    Label,
    Module,
    System,
    Key,
    createTag,
    reflectResolver,
    rotate,
    AnimatedSprite,
} from "@rez2d/core";

const SCREEN_WIDTH = 536;
const SCREEN_HEIGHT = 384;
const PADDLE_SPEED = 5;
const BALL_SPEED = 2.5;
const LABEL_X = -5;
const LABEL_Y = -4;
const BRICK_SCORE = 5;

const brickImages = ["blue", "green", "red"];

const ScoreTag = createTag("score");
const PaddleTag = createTag("paddle");
const BallTag = createTag("ball");
const BlockTag = createTag("block");
const BottomWallTag = createTag("bottom_wall");

type ExampleResources = RezResources<{
    loaded: boolean;
    score: number;
}>;

function setup(store: EntityStore<ExampleResources>, resources: Resources<ExampleResources>) {
    // Spawn all entities once assets are loaded
    const audio = resources.get("audio").unwrap();
    store.exec(resources, (commands) => {
        // Spawn the score text
        commands.create(
            ScoreTag.create(),
            Transform.create({
                pos: new Vec(-251, -187),
            }),
            Label.create({
                text: "Score 0",
                style: {
                    fill: "#ffffff",
                    fontFamily: "monospaced",
                    fontSize: 14,
                    padding: 0,
                    trim: true,
                },
                zIndex: 1,
            }),
        );

        // Spawn the paddle
        commands.create(
            PaddleTag.create(),
            Transform.create({
                pos: new Vec(0, (SCREEN_HEIGHT / 2) - 10),
            }),
            Sprite.create({
                src: "paddle.png",
            }),
            Collider.create({
                shape: Polygon.box(0, 0, 64, 20),
            }),
        );

        // Spawn ball
        commands.create(
            BallTag.create(),
            Transform.create({
                pos: new Vec(0, 0),
                scale: new Vec(2, 2),
            }),
            AnimatedSprite.create({
                src: "ball.r2s",
                name: "default",
            }),
            Collider.create({
                shape: Polygon.box(0, 0, 8, 8),
                velocity: new Vec(0, BALL_SPEED),
                resolver: reflectResolver,
            }),
        );

        // Spawn blocks
        for (let i = 0; i < 3; i++) {
            for (let x = 0; x < 5; x++) {
                for (let y = 0; y < 4; y++) {
                    commands.create(
                        BlockTag.create(),
                        Transform.create({
                            pos: new Vec(
                                32 + (i * 172) + (x * 32) - (SCREEN_WIDTH / 2),
                                24 + (y * 16) - (SCREEN_HEIGHT / 2)
                            ),
                        }),
                        Spritesheet.create({
                            src: "bricks.r2s",
                            defaultSprite: brickImages[i],
                        }),
                        Collider.create({
                            shape: Polygon.box(0, 0, 32, 16),
                        }),
                    );
                }
            }
        }

        // Spawn walls
        commands.create(
            Transform.create({
                pos: new Vec(0, -(SCREEN_HEIGHT / 2) - 1),
            }),
            Collider.create({
                shape: Polygon.box(0, 0, SCREEN_WIDTH, 2),
            }),
        );
        commands.create(
            Transform.create({
                pos: new Vec(-(SCREEN_WIDTH / 2) - 1, 0),
            }),
            Collider.create({
                shape: Polygon.box(0, 0, 2, SCREEN_HEIGHT),
            }),
        );
        commands.create(
            Transform.create({
                pos: new Vec((SCREEN_WIDTH / 2) + 1, 0),
            }),
            Collider.create({
                shape: Polygon.box(0, 0, 2, SCREEN_HEIGHT),
            }),
        );
        commands.create(
            Transform.create({
                pos: new Vec(0, (SCREEN_HEIGHT / 2) + 1),
            }),
            Collider.create({
                shape: Polygon.box(0, 0, SCREEN_WIDTH, 2),
            }),
            BottomWallTag.create(),
        );
    });

    audio.play("/assets/bgm.wav", { volume: 0.25, loop: true });
}

const Loading: System<ExampleResources> = (store, resources) => {
    if (!resources.get("loaded").unwrap()) {
        const assets = resources.get("assets").unwrap();
        if (assets.loading === 0) {
            if (assets.failed.length > 0) {
                throw new Error(`Failed loading "${assets.failed[0].filename}": ${assets.failed[0].error}`);
            }
            setup(store, resources);
            resources.set("loaded", true);
        }
    }
};

// Paddle movement system
const MovePaddle: System<ExampleResources> = (store, resources) => {
    if (resources.get("loaded").unwrap()) {
        const input = resources.get("input").unwrap();
        for (const [_id, _tag, { body }] of store.query(new Query(PaddleTag, Collider))) {
            // Move the paddle
            if (input.isKeyDown(Key.LEFT)) {
                body.velocity = new Vec(-PADDLE_SPEED, 0);
            } else if (input.isKeyDown(Key.RIGHT)) {
                body.velocity = new Vec(PADDLE_SPEED, 0);
            } else {
                body.velocity = new Vec(0, 0);
            }
        }
    }
};

// Collision system
const HandleCollision: System<ExampleResources> = (store, resources) => {
    if (resources.get("loaded").unwrap()) {
        const audio = resources.get("audio").unwrap();
        let score = resources.get("score").unwrap();
        let scoreChanged = false;
        store.exec(resources, (commands) => {
            for (const [_, { events }] of store.query(new Query(Collider))) {
                // Check all collisions this frame
                for (const collision of events.each("collision")) {
                    // The ball is the main collider we care about
                    if (store.has(collision.collider[0], BallTag)) {
                        if (store.has(collision.blocker[0], BlockTag)) {
                            // Ball hit a block
                            audio.play("/assets/brick_hit.wav");
                            commands.delete(collision.blocker[0]);
                            score += BRICK_SCORE;
                            scoreChanged = true;
                        } else if (store.has(collision.blocker[0], BottomWallTag)) {
                            // Ball hit the bottom
                            audio.stop("/assets/bgm.wav");
                            audio.play("/assets/lose.wav");
                            commands.delete(collision.collider[0]);
                        } else if (store.has(collision.blocker[0], PaddleTag)) {
                            // Ball hit the paddle
                            audio.play("/assets/ball_hit.wav");
                            // Adjust the angle the ball reflects to based on distance from the
                            // center of the paddle.
                            const dx = collision.collider[1].pos.x - collision.blocker[1].pos.x;
                            const offset = dx / (collision.blocker[1].bounds.w / 2);
                            const angle = offset * (Math.PI / 4);
                            collision.collider[1].velocity = rotate(collision.collider[1].velocity, angle);
                        } else {
                            // Ball hit top or side walls
                            audio.play("/assets/ball_hit.wav");
                        }
                    }
                }
            }
        });

        // Update score text
        if (scoreChanged) {
            resources.set("score", score);
            match(store.queryFirst(new Query(ScoreTag, Transform, Label)), {
                Some: ([_id, _tag, transform, label]) => {
                    label.setText(`Score: ${score}`);
                    transform.pos = new Vec(
                        Math.floor(-(SCREEN_WIDTH / 2) + (label.metrics.width / 2) + LABEL_X),
                        Math.floor(-(SCREEN_HEIGHT / 2) + (label.metrics.height / 2) + LABEL_Y),
                    );
                },
                None: () => {},
            });
        }
    }
};

// Game setup
const ExampleGame: Module<ExampleResources> = (_store, systems, resources) => {
    resources.set("loaded", false);
    resources.set("score", 0);
    resources
        .get("assets")
        .unwrap()
        .load("paddle.png", "ball.r2s", "bricks.r2s");
    systems
        .add(Loading)
        .add(MovePaddle)
        .add(HandleCollision);
};

const config = {
    container: document.getElementById("container") as HTMLCanvasElement,
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
    worldSize: {
        width: SCREEN_WIDTH,
        height: SCREEN_HEIGHT,
    },
    assetBaseUrl: "/assets",
};

new Rez<ExampleResources>(config).start(ExampleGame);
